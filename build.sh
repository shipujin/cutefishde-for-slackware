#!/bin/bash

for i in \
mujs \
lua \
libplacebo \
libass \
mpv \
lxqt-build-tools \
libqtxdg \
fishui \
libcutefish \
cutefish-core \
cutefish-settings \
cutefish-dock \
cutefish-launcher \
cutefish-filemanager \
cutefish-qt-plugins \
cutefish-statusbar \
cutefish-kwin-plugins \
cutefish-calculator \
cutefish-wallpapers \
cutefish-icons \
cutefish-videoplayer \
cutefish-terminal \
cutefish-screenlocker \
cutefish-sddm-theme \
sxhkd \
; do
cd $i || exit 1
./$i.SlackBuild || exit 1
cd ..
done
