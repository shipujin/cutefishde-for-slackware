This project based on [CuteFishOS](https://cutefishos.com/) &  [Slackware 15 / Current ](https://docs.slackware.com/slackware:current) 




# Installing

**Preparation**

Slackware 15 / current basic install 

http://slackware.uk/people/alien-current-iso/

You just have to install Slackware current with the standard selected packages.


**Build instruction**


_Stable_

```
$ git clone https://gitlab.com/slackernetuk/cutefishde-for-slackware.git -b stable-15.0
$ cd cutefishde-for-slackware.git
$ ./build.sh
```


_Devel_

```
$ git clone https://gitlab.com/slackernetuk/cutefishde-for-slackware.git
$ cd cutefishde-for-slackware.git
$ ./build.sh
```

Add to /etc/slackpkg/blacklist file

`[0-9]+_snuk`

`xwmconfig` to setup cutefish as standard-session



**ScreenShots**

![Screenshot_20211009_021901](/uploads/cb026f52da72c9368cf5b58fcda317fb/Screenshot_20211009_021901.png)



